﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneClient.Models
{
    public class UnitPointList
    {
        public List<UnitPoint> markers { get; set; } 
    }
}
