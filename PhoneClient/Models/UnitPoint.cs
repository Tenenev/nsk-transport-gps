﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneClient.Models
{
    public class UnitPoint
    {
        public string title { get; set; }
        public RouteType id_typetr { get; set; }
        public string marsh { get; set; }
        public int graph { get; set; }
        public char direction { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string time_nav { get; set; }
        public short azimuth { get; set; }
        public string rasp { get; set; }
        public short speed { get; set; }
    }
}
