﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneClient.Models
{
    public class RouteList
    {
        public RouteType type { get; set; }

        public List<Route> ways { get; set; } 
    }
}
