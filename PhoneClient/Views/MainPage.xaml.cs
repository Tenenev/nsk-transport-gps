﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Toolkit;
using Microsoft.Phone.Shell;
using PhoneClient.Resources;

namespace PhoneClient.Views
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            DataContext = App.ViewModel;

            InitializeComponent();
        }

        private void map_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "74bb1cec-9242-456c-a2d2-407451b6b316";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "Tfd0mESxRSYO1NY8UPwPTQ";

            var itemCollection = MapExtensions.GetChildren(map).OfType<MapItemsControl>().FirstOrDefault();
            if(itemCollection != null)
                itemCollection.ItemsSource = App.ViewModel.UnitPoints;
        }

        private void appBarAdd_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/RoutesManagePage.xaml", UriKind.Relative));
        }

        private void appBarSettings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/SettingsPage.xaml", UriKind.Relative));
        }
    }
}