﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PhoneClient.Views
{
    public partial class RoutesManagePage : PhoneApplicationPage
    {
        public RoutesManagePage()
        {
            DataContext = App.ViewModel;

            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            TbSearch.Focus();
        }

        private void TbSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null) return;
            var bindingExpr = textBox.GetBindingExpression(TextBox.TextProperty);
            bindingExpr.UpdateSource();
        }

        private void BtnHideKeyboard(object sender, RoutedEventArgs e)
        {
            LbSearchResults.Focus();
        }
    }
}