﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Reactive;
using Newtonsoft.Json;
using PhoneClient.Commands;
using PhoneClient.Common;
using PhoneClient.Models;

namespace PhoneClient.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly Uri _routeListUrl = new Uri(@"http://maps.nskgortrans.ru/listmarsh.php?r&r=true", UriKind.Absolute);
        private readonly string _unitGetCoordBaseUrl = @"http://maps.nskgortrans.ru/markers.php?r=";

        #region Properties

        #region IsLoading

        private bool _isLoading;

        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                propertyChanged("IsLoading");
            }
        }

        #endregion

        #region LoadingText

        private string _loadingText;

        public string LoadingText
        {
            get
            {
                return _loadingText;
            }
            set
            {
                _loadingText = value;
                propertyChanged("LoadingText");
            }
        }

        #endregion

        #region MapCenter

        private GeoCoordinate _mapCenter;

        public GeoCoordinate MapCenter
        {
            get
            {
                return _mapCenter;
            }
            set
            {
                _mapCenter = value;
                propertyChanged("MapCenter");
            }
        }

        #endregion

        #region ZoomLevel

        private double _zoomLevel;

        public double ZoomLevel
        {
            get
            {
                return _zoomLevel;
            }
            set
            {
                _zoomLevel = value;
                propertyChanged("ZoomLevel");
            }
        }

        #endregion

        #region MapCartographicMode

        private MapCartographicMode _mapCartographicMode;

        public MapCartographicMode MapCartographicMode
        {
            get
            {
                return _mapCartographicMode;
            }
            set
            {
                _mapCartographicMode = value;
                propertyChanged("MapCartographicMode");
            }
        }

        #endregion

        #region MyPosition

        private GeoCoordinate _myPosition;

        public GeoCoordinate MyPosition
        {
            get
            {
                return _myPosition;
            }
            set
            {
                _myPosition = value;
                propertyChanged("MyPosition");
            }
        }

        #endregion

        public ObservableCollection<RouteViewModel> Routes { get; set; }

        public ObservableCollection<RouteViewModel> SelectedRoutes { get; set; } 

        public ObservableCollection<UnitPointViewModel> UnitPoints { get; set; }


        #region Routes Search


        #region RoutesSearchText

        private string _routesSearchText;

        public string RoutesSearchText
        {
            get
            {
                return _routesSearchText;
            }
            set
            {
                _routesSearchText = value;
                propertyChanged("RoutesSearchText");

                RoutesSearchResults.Clear();
                if(!string.IsNullOrWhiteSpace(value))
                    foreach (var route in Routes.Where(r => SelectedRoutes.All(r2 => r2 != r) && r.Name.ToLower().IndexOf(value.ToLower(), StringComparison.Ordinal) != -1))
                        RoutesSearchResults.Add(route);
            }
        }

        #endregion


        #region SelectedItemInSelectedRoutes

        public RouteViewModel SelectedItemInSelectedRoutes
        {
            get
            {
                return null;
            }
            set
            {
                propertyChanged("SelectedItemInSelectedRoutes");

                // Добавляем элемент в коллекцию выбранных


                var result = MessageBox.Show(string.Format("Удалить маршрут {0}?", value.Name), "Удаление маршрута", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    SelectedRoutes.Remove(value);
                }
            }
        }

        #endregion

        public ObservableCollection<RouteViewModel> RoutesSearchResults { get; set; }

        #region RoutesSearchSelectedItem

        public RouteViewModel RoutesSearchSelectedItem
        {
            get
            {
                return null;
            }
            set
            {
                propertyChanged("RoutesSearchSelectedItem");

                // Добавляем элемент в коллекцию выбранных

                SelectedRoutes.Add(value);

                RoutesSearchText = "";
            }
        }

        #endregion

        #endregion

        #endregion


        public MainViewModel()
        {
            Routes = new ObservableCollection<RouteViewModel>();
            SelectedRoutes = new ObservableCollection<RouteViewModel>();
            UnitPoints = new ObservableCollection<UnitPointViewModel>();
            RoutesSearchResults = new ObservableCollection<RouteViewModel>();

            MapCenter = new GeoCoordinate(55.033333, 82.916667);
            ZoomLevel = 10d;
            MapCartographicMode = MapCartographicMode.Road;

            new Thread(LoadRoutes).Start();

            GetMyLocation();
        }

        public void GetMyLocation()
        {
            var watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High)
            {
                MovementThreshold = 1
            };
            watcher.PositionChanged += watcher_PositionChanged;
            watcher.Start();
        }

        private void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            MyPosition = e.Position.Location;
        } 

        public void LoadRoutes()
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                LoadingText = "Загрузка маршрутов";
                IsLoading = true;
            });

            var client = new WebClient();
            client.DownloadStringCompleted += client_DownloadRoutesCompleted;
            client.DownloadStringAsync(_routeListUrl);
        }

        private void client_DownloadRoutesCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    IsLoading = false;
                });
                
                return;
            }


            var routeLists = JsonConvert.DeserializeObject<List<RouteList>>(e.Result);

            var routes = new List<RouteViewModel>();
            foreach (var routeList in routeLists)
                routes.AddRange(
                    routeList.ways.Select(
                        w =>
                            new RouteViewModel
                            {
                                Id = w.marsh,
                                Name = w.name,
                                Type = routeList.type + 1,
                                From = w.stopb,
                                To = w.stope
                            }));

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                foreach (var route in routes)
                    Routes.Add(route);

                IsLoading = false;

                var timer = Observable.Interval(TimeSpan.FromSeconds(5));
                timer.Subscribe(tick => UpdateUnitPoints());
            });
        }

        public void UpdateUnitPoints()
        {
            if (SelectedRoutes.Count == 0)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => UnitPoints.Clear());
                return;
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                LoadingText = "Обновление расположения транспорта";
                IsLoading = true;
            });

            var client = new WebClient();
            client.DownloadStringCompleted += client_DownloadUnitPointsCompleted;

            client.DownloadStringAsync(
                new Uri(_unitGetCoordBaseUrl +
                        string.Join("",
                            SelectedRoutes.Select(
                                r => string.Format("{0}-{1}-W-{2}%257C", (int) r.Type, r.Id, r.Name)))));
        }

        private void client_DownloadUnitPointsCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    IsLoading = false;
                });

                return;
            }

            var unitPointModels = JsonConvert.DeserializeObject<UnitPointList>(e.Result);

            var unitPoints =
                unitPointModels.markers.Select(
                    m =>
                        new UnitPointViewModel
                        {
                            Position = new GeoCoordinate(m.lat, m.lng),
                            Id = m.marsh,
                            Type = m.id_typetr,
                            Name = m.title,
                            ScheduleId = m.graph,
                            Azimuth = m.azimuth
                        });

            // TODO: Проверить поинты на одинаковый CompositeId

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                // Удаляем юниты, которыйх нет в новом списке
                foreach (
                    var unitPoint in
                        UnitPoints.ToList()
                            .Where(unitPoint => unitPoints.All(p => p.CompositeId != unitPoint.CompositeId)))
                    UnitPoints.Remove(unitPoint);

                foreach (var unitPoint in unitPoints)
                {
                    var existingUnitPoint = UnitPoints.SingleOrDefault(p => p.CompositeId == unitPoint.CompositeId);
                    if (existingUnitPoint == null)
                        UnitPoints.Add(unitPoint);
                    else
                    {
                        existingUnitPoint.Position = unitPoint.Position;
                    }
                }
                    
                IsLoading = false;
            });
        }

        #region Commands

        //private DelegateCommand _openSettingsCommand;

        //public ICommand OpenSettingsCommand
        //{
        //    get
        //    {
        //        return _openSettingsCommand ??
        //               (_openSettingsCommand =
        //                   new DelegateCommand(param => OpenSettings(), param => CanOpenSettings()));
        //    }
        //}

        //private void OpenSettings()
        //{
        //    MessageBox.Show("Hello");
        //}

        //private bool CanOpenSettings()
        //{
        //    return true;
        //}

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void propertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
