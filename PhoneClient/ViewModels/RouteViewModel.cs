﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneClient.Models;

namespace PhoneClient.ViewModels
{
    public class RouteViewModel
    {
        public string Id { get; set; }

        public RouteType Type { get; set; }

        public string Name { get; set; }

        public string From { get; set; }

        public string To { get; set; }
    }
}
