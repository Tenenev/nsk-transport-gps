﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneClient.Models;

namespace PhoneClient.ViewModels
{
    public class UnitPointViewModel : INotifyPropertyChanged
    {
        private GeoCoordinate _position;

        public GeoCoordinate Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                propertyChanged("Position");
            }
        }

        public string Id { get; set; }

        public RouteType Type { get; set; }

        public string Name { get; set; }

        public int ScheduleId { get; set; }

        public short Azimuth { get; set; }

        public string CompositeId
        {
            get
            {
                return string.Format("{0}|{1}|{2}", Id, (int) Type, ScheduleId);
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void propertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
