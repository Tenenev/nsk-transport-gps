﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PhoneClient.Converters
{
    public class AzimuthToAngleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(!(value is short)) throw new ArgumentException("Azimuth is not short type");

            var azimuth = (short) value;

            var angle = 360d - azimuth + 90d;
            //if (angle > 360d)
            //    angle -= 360d;
            return angle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
