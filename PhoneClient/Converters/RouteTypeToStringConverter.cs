﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using PhoneClient.Models;

namespace PhoneClient.Converters
{
    public class RouteTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is RouteType)) throw new ArgumentException("Value is not RouteType");

            switch ((RouteType)value)
            {
                case RouteType.Bus:
                    return "автобус";
                case RouteType.Trol:
                    return "троллейбус";
                case RouteType.Tram:
                    return "трамвай";
                case RouteType.Taxi:
                    return "такси";
                default:
                    return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
