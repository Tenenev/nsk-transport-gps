﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhoneClient.Commands
{
    public class DelegateCommand : ICommand
    {
        Func<object, bool> _canExecute;
        Action<object> _executeAction;

        Func<bool> _canExecuteSimple;
        Action _executeActionSimple;

        public DelegateCommand(Action<object> executeAction)
            : this(executeAction, null)
        {
        }

        public DelegateCommand(Action executeAction)
            : this(executeAction, null)
        {
        }

        public DelegateCommand(Action<object> executeAction, Func<object, bool> canExecute)
        {
            if (executeAction == null)
            {
                throw new ArgumentNullException("executeAction");
            }
            _executeAction = executeAction;
            _canExecute = canExecute;
        }

        public DelegateCommand(Action executeAction, Func<bool> canExecute)
        {
            if (executeAction == null)
            {
                throw new ArgumentNullException("executeAction");
            }
            _executeActionSimple = executeAction;
            _canExecuteSimple = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            var result = true;
            var canExecuteHandler = _canExecute;
            if (canExecuteHandler != null)
            {
                result = canExecuteHandler(parameter);
                return result;
            }

            Func<bool> canExecuteHandlerSimple = _canExecuteSimple;
            if (canExecuteHandlerSimple != null)
            {
                result = canExecuteHandlerSimple();
            }

            return result;
        }

        public bool CanExecute()
        {
            var result = true;
            Func<bool> canExecuteHandler = _canExecuteSimple;
            if (canExecuteHandler != null)
            {
                result = canExecuteHandler();
            }

            return result;
        }


        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public void Execute(object parameter)
        {
            // Default to action that takes parameter.
            if (_executeAction != null)
            {
                _executeAction(parameter);
                return;
            }

            // Fallback to parameterless delegate.
            if (_executeActionSimple != null)
            {
                _executeActionSimple();
                return;
            }
        }

        public void Execute()
        {
            _executeActionSimple();
        }
    }
}
